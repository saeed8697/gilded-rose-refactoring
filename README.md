# Welcome to Gilde Rose Refactor !
<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="https://raw.githubusercontent.com/DXHeroes/knowledge-base-content/master/files/refactoring.png" alt="Logo" width="150px
" height="90">
  </a>

<h3 align="center">Gilded Rose Refactoring </h3>

  <p align="center">
    for more info 
    <br />
    <a href="https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/main/GildedRoseRequirements.txt"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gildedrefactor.vercel.app/"> <strong>(( !for visit PRODUCTION (DEMO) click Here !)) </strong> </a>
    ·
    <a href="https://github.com/emilybache/GildedRose-Refactoring-Kata">Ref Source </a>
    ·
    <a href="https://www.linkedin.com/in/saeed-hasanabadi-79081b183/">About Me </a>
  </p>
</div>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#testing">testing</a></li>
      </ul>
    </li>
    <li><a href="#about">About Me </a></li>

  </ol>
</details>

## About The Project 
<div id="about-the-project"></div>


The Task is to refactor the  From [Gilded Rose Refactoring Kata](https://saeedhasanabadi.herokuapp.com) 
> Gilded Rose Requirements Specification
>
> Hi and welcome to team Gilded Rose. As you know, we are a small inn with a prime location in a
> prominent city ran by a friendly innkeeper named Allison. We also buy and sell only the finest goods.
> Unfortunately, our goods are constantly degrading in quality as they approach their sell by date. We
> have a system in place that updates our inventory for us. It was developed by a no-nonsense type named


My Solution  is to fix the problem with usage of Strategy design pattern  and implement it more effective and conceptual 
.
the goal was as always make code semantically correct and readable , 
Working with styled component , react hooks(useCallback,UseMemo,UseEffect,UseContext,UseState,UseRef) , framer motion , leventhtin Algo for searching  , ...
### Built With
<div id="built-with"></div>

<img src="https://camo.githubusercontent.com/2739bd6e1b2c420b8956e83e9f5729c3abb2381a8f4fc907a526039eb7f640ae/68747470733a2f2f62616467656e2e6e65742f62616467652f2d2f547970655363726970742f626c75653f69636f6e3d74797065736372697074266c6162656c">      <img src="https://img.shields.io/badge/react%20-%2320232a.svg?&style=for-the-badge&logo=react&logoColor=%2361DAFB">  <hr>

* [React.js](https://reactjs.org/)
* [Styled Components](https://styled-components.com/)
* [TypeScript](https://www.typescriptlang.org/)

## Getting Started

<div id="#getting-started"></div>

##Step 1 - Dependencies

You will need:

* [Git](http://git-scm.com/downloads)
* [node](https://nodejs.org/)
* [yarn](https://yarnpkg.com/en/docs/install) (Optional. Not Required if you use NPM)

Please install them if you don't have them already.

## Step 2 - Clone the repository:
<div id="prerequisites"></div>

From the command line, clone the repository:

```sh
$ git clone https://gitlab.com/saeed8697/gilded-rose-refactoring.git
```

## Step 3 Install Each Lesson

Each section is a separate app using [Create React App](https://github.com/facebookincubator/create-react-app). This repo has a script to install all of them at once.

If you are using yarn run from the root of the repository:

```sh
yarn
```

If you are using npm, run from the root of the repository:

```sh
npm run install
```
<div id="installation"></div>

## Step 4 - Run an app

Once the dependencies are installed, you can run the app for a lesson:

```sh
yarn start
# or
npm run start
```
## Step 5 - Build your Application 

for production and building your application :

```sh
yarn build 
# or
npm run build  



```
## Testing  -  

for run  tests just run  :
<div id="testing"></div>

```sh
yarn test 
# or
npm run start  

```
Your browser should open up to a running app.


# Hey there ! Saeed Hasanabadi  ! &emsp;  <img src="https://raw.githubusercontent.com/kvssankar/kvssankar/main/programmer.gif" width="350px">
<div id="about"></div>



### How to reach me: <strong>(Click the badge to view my profiles !)</strong>

<img src="https://img.shields.io/badge/saeedh582@gmail.com-%23D14836.svg?&style=for-the-badge&logo=gmail&logoColor=white" href="mailto:saeedh582@gmail.com">   <a  href="https://saeedhasanabadi.herokuapp.com"><img src="https://img.shields.io/badge/@Website -%23E4405F.svg?&style=for-the-badge&logo=website&logoColor=white"></a>   <a href="https://www.linkedin.com/in/saeed-hasanabadi-79081b183/"><img src="https://img.shields.io/badge/saeed-hasanabadi-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" ></a>  
<hr>






#### Thank You-🙏🏼

⭐️ From [Saeed](https://saeedhasanabadi.herokuapp.com)