import {ItemModel} from "./ItemModel";
//TODO implement highest quality

/***
 *
 * implement strategy design pattern for reducing interrupted if else condition
 */
export  interface Strategy {
    updateItemQuality : (ItemModel : ItemModel) => void

}