export enum ItemModelProductTypes {

    Normal = 'Normal',
    AgedBrie = 'AgedBrie',
    Sulfuras = 'Sulfuras',
    BackstagePass = 'BackstagePass',
    Conjured = 'Conjured'

}

// custom  color for each type
/**
 *
 * aged = >#266678
 * sulfuras = > #cb7c7a
 * backstage = > #36a18b
 * conjured = > #cda35f
 * Normal => #747474
 *
 *
 */
export  const CARD_COLORS = ["#266678", "#cb7c7a", "#36a18b", "#cda35f", "#9a0000",];




export class ItemModel {
    name: string = ItemModelProductTypes.Normal;
    quality: number = 0;
    sellIn: number = 0;
    id: number = 0;

    constructor(b: Partial<ItemModel> = {}) {
        Object.assign(this, b);
    }

    //get type of each item
    getItem() :ItemModelProductTypes {
        let trimmed = this.name.toLowerCase().trim();

        if (trimmed.includes('aged')) {
            return ItemModelProductTypes.AgedBrie;
        } else if (trimmed.includes('sulfuras')) {
            return ItemModelProductTypes.Sulfuras;
        } else if (trimmed.includes('backstage')) {
            return ItemModelProductTypes.BackstagePass;
        } else if (trimmed.includes('conjured')) {
            return ItemModelProductTypes.Conjured;
        } else {
            return ItemModelProductTypes.Normal;
        }
    }
    //get color  of each item

    getColor() :string {
        let trimmed = this.name.toLowerCase().trim();

        if (trimmed.includes('aged')) {
            return CARD_COLORS[0];
        } else if (trimmed.includes('sulfuras')) {
            return CARD_COLORS[1];
        } else if (trimmed.includes('backstage')) {
            return CARD_COLORS[2];
        } else if (trimmed.includes('conjured')) {
            return CARD_COLORS[3];
        } else {
            return CARD_COLORS[4];
        }
    }


    //description for each type
    itemString(): string {
        return `this is sample for product ${this.name} , with quality of ${this.quality}`
    }

    //description for each type
    toString(): string {
        return this.name.toLowerCase()+' _ ' + this.quality+' _ ' + this.sellIn + ' _ ' + this.id
    }
}


/***
 *
 * custom input  product
 */
export const initialState: Array<ItemModel> = [
    new ItemModel({id :0 , name: 'Backstage ', quality: 0, sellIn: 30}),
    new ItemModel({id : 1, name: 'Conjured', quality: 10, sellIn: 49}),
    new ItemModel({id : 2, name: 'Test Item 1', quality: 12, sellIn: 0}),
    new ItemModel({id : 3, name: 'Aged Brie', quality:2, sellIn: 5}),
    new ItemModel({id : 4, name: 'Sulfuras', quality: 28, sellIn: 5}),
];
