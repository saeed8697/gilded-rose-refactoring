import {Strategy} from "./Strategy";
import {ItemModel} from "./ItemModel";



export  class WrapperClass {

    Strategy : Strategy;

    constructor(Strategy: Strategy) {
        this.Strategy = Strategy;

    }
    updateItemQuality(ItemModel : ItemModel): void {
        this.Strategy.updateItemQuality(ItemModel)
    }

}