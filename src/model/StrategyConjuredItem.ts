import {Strategy} from "./Strategy";
import {ItemModel} from "./ItemModel";

export class StrategyConjuredItem implements Strategy {
    updateItemQuality(item: ItemModel): void {
        this.decreaseQuality(item);
        this.decreaseSellIn(item);
        if (this.passedSellIn(item)) {
            this.decreaseQuality(item);
        }
    }
    passedSellIn(itemModel : ItemModel):boolean{
        return itemModel.sellIn <  0
    }

    decreaseSellIn(itemModel:ItemModel):void{
        itemModel.sellIn =itemModel.sellIn-1

    }
    decreaseQuality(item:ItemModel):void{
        item.quality -= this.getDegradationFactor(item);

    }

    getDegradationFactor(item : ItemModel):number{
        if (item.quality > 1) {
            return 2;
        }
        if (item.quality > 0) {
            return 1;
        }
        return 0;
    }







}
