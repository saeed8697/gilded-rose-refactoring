import {Strategy} from "./Strategy";
import {ItemModel} from "./ItemModel";


export class StrategyAged implements Strategy {

    HIGHEST_QUALITY: number = 50;

    updateItemQuality(item: ItemModel): void {
        this.increaseQuality(item);
        this.decreaseSellIn(item);
        if (this.passedSellIn(item)) {
            this.increaseQuality(item);
        }
    }

    passedSellIn(itemModel: ItemModel): boolean {
        return itemModel.sellIn < 0
    }

    decreaseSellIn(itemModel: ItemModel): void {
        itemModel.sellIn = itemModel.sellIn - 1

    }

    increaseQuality(item: ItemModel): void {

        item.quality = Math.min(Math.max(item.quality + 1, 0), this.HIGHEST_QUALITY)

    }
}

