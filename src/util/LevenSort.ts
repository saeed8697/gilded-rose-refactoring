
import levensrc from "./leven";

var bufChar = String.fromCharCode(2000)


/***
 *  for sorting array based on  search with implementing leven algo
 *
 * @param ary = ['code ' , ' codeazure ']
 * @param src1 = ' codeazure '
 * output = [' codeazure ' , 'code ' ]
 */

export  default  function levenSort(ary: string[], src1:string)  : string[] {
    let max = 0;
    ary.forEach(function (el) {
        if (!el) return
        if (el.length > max) max = el.length

    })

    const maximize = function (val: string) {
        if (!val || val.length === max) return val
        return val + Array(max - val.length).join(bufChar)
    };

    const maxDist = function (a: string, b: string) {
        const aLen = (a || '').length;
        const bLen = (b || '').length;

        return (aLen > bLen ? aLen : bLen) || 1
    };

    const leven = function (a: string, b: string) {
        if (a === b) return 0

        b = maximize(b)

        if (!a || !b) return maxDist(a, b)

        return levensrc(a, b)
    };

    const levSort = function (src: string, a: string, b: string) {

        if (!a) return 1
        if (!b) return -1
        let c: number;
        let d: number;
        c = leven(src, a)
        d = leven(src, b)

        return c - d
    };

    return ary.sort(function (a, b) {

        return levSort(src1, a, b)


    })
}