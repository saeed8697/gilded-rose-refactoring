import {ItemModel, ItemModelProductTypes} from "../model/ItemModel";
import {Strategy} from "../model/Strategy";
import {StrategyAged} from "../model/StrategyAged";
import {StrategyBackstaged} from "../model/StrategyBackstaged";
import {StrategyStandardItem} from "../model/StrategyStandardItem";
import {StrategyLegendaryItem} from "../model/StrategyLegendaryItem";
import {WrapperClass} from "../model/WrapedStrategy";
import {StrategyConjuredItem} from "../model/StrategyConjuredItem";

export class GildeRoseUtil {
    items: Array<ItemModel>;

    constructor(items = [] as Array<ItemModel>) {
        this.items = items;
    }

    updateQuality(): void{
        for (var item  of   this.items) {
            this.updateItemQuality(item);
        }
    }


    updateItemQuality = (item: ItemModel): void => {

        let type = new ItemModel(item).getItem()
        let appropriateStrategy: Strategy;

        switch (type) {

            case ItemModelProductTypes.AgedBrie:
                appropriateStrategy = new StrategyAged();
                break;

            case ItemModelProductTypes.BackstagePass:
                appropriateStrategy = new StrategyBackstaged();
                break;

            case ItemModelProductTypes.Conjured:
                appropriateStrategy = new StrategyConjuredItem();
                break;

            case ItemModelProductTypes.Sulfuras:
                appropriateStrategy = new StrategyLegendaryItem();
                break;

            default:
                appropriateStrategy = new StrategyStandardItem();

        }
        let wrappedItem = new WrapperClass(appropriateStrategy);
        wrappedItem.updateItemQuality(item);
    };

}

export  function getgildeQuantitylist(items = [] as Array<ItemModel>) {

    const gildeRoseUtil = new GildeRoseUtil(items)
    gildeRoseUtil.updateQuality();
    return gildeRoseUtil.items

}



