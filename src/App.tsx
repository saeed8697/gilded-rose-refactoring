import {  Container } from './components';
import styled from 'styled-components'
import { GildedProvider } from './contexts/GildedContext';
import { MessageProvider } from './contexts/MessageContext';
import Sidebar from "./components/SideBar";


function App() {

  return (
    <GildedProvider>
      <MessageProvider>
        <HeroContainer>
          <Container />
        </HeroContainer>
        <Sidebar currentPath={'/dashboard'} />

      </MessageProvider>
    </GildedProvider>
  );
}

export default App;


const HeroContainer = styled.div`
  @media only screen and (min-width: 600px){
    margin-left: 4.5rem;
    padding: 2rem;


  }
`;