import {GildeRoseUtil} from "../util/GildeRoseUtil";
import {ItemModel, ItemModelProductTypes} from "../model/ItemModel";
import levensrc from "../util/leven";
import {min} from "lodash";
import levenSort from "../util/LevenSort";


describe('#gildetest', () => {
    describe('updatequality', () => {
        describe('and the quality <  min value', () => {
            it('sets the quality zero', () => {

                const gildedRose = new GildeRoseUtil([new ItemModel({
                    name: ItemModelProductTypes.Normal,
                    quality: -10,
                    sellIn: 0
                })]);

                gildedRose.updateQuality();

                expect(gildedRose.items[0].quality).toBe(0);
            });
        });

        describe('and the quality is greater than maximum value', () => {
            it('sets the quality as the maximum value', () => {
                const initialState = [new ItemModel({
                    name: ItemModelProductTypes.AgedBrie,
                    sellIn: 15,
                    quality: 56,
                })
                ];


                const gildedRose = new GildeRoseUtil(initialState);

                gildedRose.updateQuality();

                expect(gildedRose.items[0].quality).toBe(50);
            });
        });
        describe('decrease by 2', () => {
            describe('sell < 0 quality should minus + 2 or -2', () => {
                it('minus + 2', () => {
                    const initialState = [new ItemModel({
                        name: ItemModelProductTypes.AgedBrie,
                        sellIn: -2,
                        quality: 10,
                    },)

                    ];


                    const gildedRose = new GildeRoseUtil(initialState);

                    gildedRose.updateQuality();

                    expect(gildedRose.items[0].quality).toBe(12);
                });

                it('minus - 2', () => {
                    const initialState = [new ItemModel({
                        name: ItemModelProductTypes.Normal,
                        sellIn: -2,
                        quality: 10,
                    },)

                    ];


                    const gildedRose = new GildeRoseUtil(initialState);

                    gildedRose.updateQuality();

                    expect(gildedRose.items[0].quality).toBe(8);
                });
            });

        });
        describe('step 1', () => {
            it('decrease sell in', () => {
                const initialState = [new ItemModel({
                    name: ItemModelProductTypes.AgedBrie,
                    sellIn: 2,
                    quality: 10,
                })

                ];


                const gildedRose = new GildeRoseUtil(initialState);

                gildedRose.updateQuality();

                expect(gildedRose.items[0].sellIn).toBe(1);
            });

        });

        describe('BACKSTAGE_PASSES', () => {
            describe('run 3 test for 3 types of day', () => {
                describe('and the sell in is less or equals than days to increase three times', () => {
                    it('increases the quality by 3', () => {
                        const initialState = [new ItemModel({
                            name: ItemModelProductTypes.BackstagePass,
                            sellIn: 4,
                            quality: 10,
                        })

                        ];


                        const gildedRose = new GildeRoseUtil(initialState);

                        gildedRose.updateQuality();

                        expect(gildedRose.items[0].quality).toBe(13);
                    });
                    it('increases the quality by 2', () => {
                        const initialState = [new ItemModel({
                            name: ItemModelProductTypes.BackstagePass,
                            sellIn: 6,
                            quality: 11,
                        })

                        ];


                        const gildedRose = new GildeRoseUtil(initialState);

                        gildedRose.updateQuality();

                        expect(gildedRose.items[0].quality).toBe(13);
                    });
                    it('increases the quality by 1', () => {
                        const initialState = [new ItemModel({
                            name: ItemModelProductTypes.BackstagePass,
                            sellIn: 12,
                            quality: 12,
                        })

                        ];


                        const gildedRose = new GildeRoseUtil(initialState);

                        gildedRose.updateQuality();

                        expect(gildedRose.items[0].quality).toBe(13);
                    });

                });
            });
        });
        describe('legendary" item', () => {
            it('does not changes any value', () => {
                const initialState = [new ItemModel({
                    name: ItemModelProductTypes.Sulfuras,
                    sellIn: 0,
                    quality: 80,
                })

                ];

                const gildedRose = new GildeRoseUtil(initialState);

                gildedRose.updateQuality();

                expect(gildedRose.items).toMatchObject(initialState);
            });
        });

        describe('Test  Search Engine ', () => {
            it('leven', () => {
                var a = levensrc('code', 'codedazur')
                var b = levensrc('test', 'codedazur')


                expect(min([a,b]) == a).toBeTruthy();
            });
        });

        it('leven array  sort ', () => {
            var a = levenSort(['code','base','codedazur'] , 'codedazur')
            expect(a).toEqual(['codedazur','code','base']);
    });


    });
});