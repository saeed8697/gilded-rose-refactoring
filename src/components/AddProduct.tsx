import React, { useState, useContext } from 'react'
import styled from 'styled-components'
import { GildedContext } from '../contexts/GildedContext';
import {ItemModel} from "../model/ItemModel";
import {MessageContext} from "../contexts/MessageContext";

/***
 *
 * for adding  product
 *
 */
const AddProduct = () => {
    const [quality, setQuality] = useState<number>(12);
    const [name, setName] = useState<string>('Aged Brie');
    const [sellIn, setSellIn] = useState<number>(10);
    const { items, setItems } = useContext(GildedContext)
    const { AddMessage } = useContext(MessageContext)
    const addItem = (item: ItemModel): void => {
        setItems([item,...items])
    }



    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        const newItem = new ItemModel({name: name, quality: sellIn, sellIn: quality ,id: items.length} );
        addItem(newItem);
        AddMessage(newItem);
    }

    return (
         <div style={{alignSelf: "center" , padding :"16px" }}>
             <Card><Title>Add your product</Title>

             <FormContainer onSubmit={e => { handleSubmit(e) }}>
                 <InputContainer>
                     <Label>Name</Label>
                     <br />
                     <Input
                         value={name}
                         onChange={e => setName(e.target.value)}
                         name='name'
                         type='text'
                     />

                 </InputContainer>
                 <InputContainer>
                     <Label>Quality</Label>
                     <br />
                     <Input
                         value={quality}
                         onChange={e => setQuality(Number(e.target.value))}
                         name='quality'
                         type='number'
                         max={50}
                         min={0}
                     />
                 </InputContainer>
                 <InputContainer>
                     <Label>SellIn</Label>
                     <br />
                     <Input
                         value={sellIn}
                         onChange={e => setSellIn(Number(e.target.value))}
                         name='sellIn'
                         type='number'
                     />
                 </InputContainer>
                 <CreateButton
                     type='submit'
                 >Add Product</CreateButton>
             </FormContainer>
         </Card>
         </div>

    )
}


const CreateButton = styled.button`
  color: rgb(2, 132, 199);
  background-color: rgb(224, 242, 254);
  border: none;
  height: min-content;
  padding: 8px;
  margin-top: 25px;
  border-radius: 10px;
  margin-right: 0.5rem;
  
  cursor: pointer;
`;
const FormContainer = styled.form`
display: flex;
flex-direction: column;
justify-content: space-between;

`;

const InputContainer =  styled.div`
  margin: 0.4em 0;
`;



const Title =  styled.div`
  font-weight: 700;
  margin: 0.25em 0;
  padding: 0;


`;

const Card =  styled.div`
  padding: 2em;
  margin: 8%;
  @media only screen and (min-width: 600px){
    margin: 15%;
  }
  background-color: #2b2c3b;
  border-radius: 6px;
  box-shadow: 0 3px 25px 0 rgba(0, 0, 0, 0.36);

`;


const Label =  styled.label`
  color: #74757B;
  font-weight: 400;
  font-size: .8em;
  text-align: left;
  text-transform: capitalize;
  padding: .5em 0;

`;
const Input =  styled.input`
  border: none;
  border-radius: 6px;
  background-color: #5A5A6C;
  color: #ffffff;
  font-weight: 900;
  padding: 1.25em 1em;
  font-size: .825em;
  width: 90%;

`;






export default AddProduct
