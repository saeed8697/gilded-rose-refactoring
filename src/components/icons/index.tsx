import { DashboardIcon } from './dashboard'
import { ProfileIcon } from './profile'
import { Searchicon } from './searchicon'

export {
	DashboardIcon,
	ProfileIcon,
	Searchicon,
}
