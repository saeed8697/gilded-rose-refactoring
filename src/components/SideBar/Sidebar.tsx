import React from 'react'
import styles from './SideBar.module.css'
import { DashboardIcon, ProfileIcon } from '../icons'
interface ItemProps {
	currentPath: string,

}
const Sidebar = ({ currentPath  } : ItemProps) => {
	return (
		<section className={styles.sidebar}>
			<div className={styles.icons}>
				<DashboardIcon active={currentPath === '/dashboard'}/>


			</div>

			<div className={styles.icons}>
				<ProfileIcon active={currentPath === '/profile'} />

			</div>
		</section>
	)
}

export default Sidebar
