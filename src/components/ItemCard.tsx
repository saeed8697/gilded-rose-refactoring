import React, {useCallback, useContext} from "react";
import {motion} from "framer-motion";
import styled from "styled-components";
import {GildedContext} from "../contexts/GildedContext";
import {ItemModel, ItemModelProductTypes} from "../model/ItemModel";
import move from "../util/MoveToEnd";
import {MessageContext} from "../contexts/MessageContext";

const CARD_OFFSET = 12;
const SCALE_FACTOR = 0.06;


/**
 *
 * handle card info
 *
 */
const ItemCard = () => {
    const {items, setItems} = useContext(GildedContext)

    const {DeletedMessage} = useContext(MessageContext)
//use callback for memoization of array movement (lodash)
    const moveToEnd = useCallback((form) => {
        const memoized = move(items, form, items.length - 1)
        //context dispatch and should add to dependency
        setItems(memoized)
    }, [setItems,items]);



    const handleRemove = (item: ItemModel): void => {
        setItems(items.slice(1))
        DeletedMessage(item)
    }
    return (
        <WrapperStyle>
            <CardWrapStyle>
                {items.map((card, index) => {
                    const canDrag = index === 0;

                    return (
                        <CardStyle
                            color={card.getColor()}
                            canDrag={canDrag}
                            key={card.id}

                            animate={{
                                top: index * -CARD_OFFSET,
                                scale: 1 - index * SCALE_FACTOR,
                                zIndex: items.length - index
                            }}
                            drag={canDrag ? "y" : false}
                            dragConstraints={{
                                top: 0,
                                bottom: 0
                            }}
                            onDragEnd={() => moveToEnd(index)}


                        >
                            {new ItemModel(card).getItem() === ItemModelProductTypes.Normal ?
                                <Circle color={card.getColor()}/> :
                                <Triangle color={card.getColor()}/>
                            }
                            <H2>{card.name}</H2>
                            <P> {card.itemString()}</P>
                            <div>
                                <A onClick={() => handleRemove(card)}>Remove</A>
                            </div>

                            <div>
                                <Sellin color={card.getColor()}> Sell in : {card.sellIn}</Sellin>
                            </div>
                        </CardStyle>

                    );
                })}
            </CardWrapStyle>
        </WrapperStyle>
    );
};

const WrapperStyle = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`
const CardWrapStyle = styled.div`
  position: relative;
  width: calc(100%);

  @media only screen and (max-width: 600px) {
    width: calc(100% / 1.2);
  }
  height: 280px;
`
const Circle = styled.div`
  margin-top: -15%;

  height: 100%;
  width: 100%;
  border-radius: 50%;
  background-color: ${props => (props.color)};
  margin-left: 40%;

`

const Triangle = styled.div`


  height: 100%;
  width: 100%;
  background-color: ${props => (props.color)};
  border-radius: 20px;
  transform: rotate(70deg);
  margin-top: -45%;
  margin-left: 25%;

`
const H2 = styled.h2`



  position: absolute;
  left: 0;
  top: 0;
  margin: 20px 0 0 45px;
  font-size: 36px;
  font-weight: 600;
  text-transform: uppercase;
  letter-spacing: 15px;
  color: #fff;
  mix-blend-mode: difference;


`
const P = styled.p`


  position: absolute;
  left: 0;
  bottom: 0;
  margin: 0 20px 60px 20px;
  font-size: 16px;
  color: #ffffff;
  mix-blend-mode: difference;



`
const A = styled.a`


  position: absolute;
  right: 0;
  bottom: 0;
  margin: 20px;
  cursor: pointer;
  background-color: #000000;
  color: #ffffff;
  font-size: 14px;
  padding: 6px 15px;
  border-radius: 30px;
  box-shadow: 10px 0 50px rgba(0, 0, 0, 0.1);
`
const Sellin = styled.a`


  position: absolute;
  left: 0;
  bottom: 0;
  margin: 20px;
  cursor: pointer;
  background-color: ${props => (props.color)};
  color: #ffffff;
  font-size: 14px;
  padding: 6px 15px;
  border-radius: 30px;
  box-shadow: 10px 0 50px rgba(0, 0, 0, 0.1);
`


const CardStyle = styled(motion.li)<{ canDrag: boolean }>`
  position: absolute;
  width: calc(100%);
  overflow: hidden;

  @media only screen and (max-width: 600px) {
    width: calc(100% / 1.02);
  }
  height: 280px;
  border-radius: 8px;
  transform-origin: top center;
  background-color: ghostwhite;
  cursor: ${p => (p.canDrag ? "grab" : "auto")};
`;


export default ItemCard
