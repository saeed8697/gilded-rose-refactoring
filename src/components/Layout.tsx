
import {useState, useContext, useMemo} from 'react';
import { AddProduct} from './index';
import styled from 'styled-components';
import { GildedContext } from '../contexts/GildedContext';
import {getgildeQuantitylist} from "../util/GildeRoseUtil";
import ItemCard from "./ItemCard";
import SearchBar from "./SearchBar";

/**
 * base layout of page
 *
 */
const Layout = () => {
    const { items, setItems } = useContext(GildedContext)

    const [days, setDays] = useState(0)

    // day count using memo for value memoization
    const retrivememoizedDate = useMemo(() => addDays(days, new Date()), [days]);



    function addDays(numOfDays: number, date = new Date()) {
        date.setDate(date.getDate() + numOfDays);

        return date;
    }
    const updateItems = () => {

        setDays (days+1);
        setItems(getgildeQuantitylist(items));
    }

    return (
        <Wrapper>
            <HeaderContainer>
                <SearchBar/>

                <DayCounter> Date : {retrivememoizedDate.toDateString()}   </DayCounter>
                <UpdateItemsButton onClick={updateItems}>Update Quality</UpdateItemsButton>

            </HeaderContainer>
            <ItemsContainer>
                <ItemCard />
                <AddProduct />

            </ItemsContainer>

        </Wrapper>
    )
}

export default Layout

const ItemsContainer = styled.div`
  mix-blend-mode: overlay;
  
  
  @media only screen and (min-width: 600px){
    display: grid;

    grid-template-columns: repeat(auto-fill, minmax(calc(100% / 2.5), 1fr));
    grid-auto-rows: minmax(150px, auto);
    grid-gap: calc(100% / 6);

  }
  @media only screen and (max-width: 600px){
    width: 100%;
    display: block;

  }
  margin-bottom: 2rem;
`;

const UpdateItemsButton = styled.button`
  color: rgb(224,242,254);
  background-color: rgb(2,132,199);
  border: none;
  height: min-content;
  padding: 8px;
  font-weight: 600;
  align-self: center;
  margin: 12px;
  border-radius: 10px;
  cursor: pointer;
  width: 120px;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
`;

const DayCounter = styled.h2``;

const HeaderContainer = styled.section`
  padding: 2em 6.5em;
  background-color: #2B2C3B;
  display: inline-flex;
  justify-content: space-between;
  box-shadow: 0 3px 25px 0 rgba(0,0,0,0.36);
  @media only screen and (max-width: 600px){
     
      display : block;
      text-align: center;
      padding: 2em 0.5em 2em 0.5em;

    

  }
`