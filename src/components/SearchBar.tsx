import React, {useState, useRef, useEffect, MutableRefObject, useContext} from "react";
import styled from "styled-components";
import {Searchicon} from "./icons";
import levenSort from "../util/LevenSort";
import {GildedContext} from "../contexts/GildedContext";
import {ItemModel} from "../model/ItemModel";


const Form = styled.form<{ barOpened?: boolean }>`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
  background-color: #37474f;
  
  /* Change width of the form depending if the bar is opened or not */
  width: ${props => (props.barOpened ? "25rem" : "1.5rem")};

  @media only screen and (max-width: 600px){

    width: ${props => (props.barOpened ? "90%" : "1.5rem")};
    

  }
  /* If bar opened, normal cursor on the whole form. If closed, show pointer on the whole form so user knows he can click to open it */
  cursor: ${props => (props.barOpened ? "auto" : "pointer")};
  padding: 1rem;
  height: 1.5rem;
  border-radius: 10rem;
  transition: width 300ms cubic-bezier(0.645, 0.045, 0.355, 1);
`;

const Input = styled.input<{ barOpened?: boolean }>`
  font-size: 14px;
  line-height: 1;
  background-color: transparent;
  width: 100%;
  margin-left: ${props => (props.barOpened ? "1rem" : "0rem")};
  border: none;
  color: white;
  transition: margin 300ms cubic-bezier(0.645, 0.045, 0.355, 1);

  &:focus,
  &:active {
    outline: none;
  }
  &::placeholder {
    color: white;
  }
`;

const Button = styled.button<{ barOpened?: boolean }>`
  line-height: 1;
  pointer-events: ${props => (props.barOpened ? "auto" : "none")};
  cursor: ${props => (props.barOpened ? "pointer" : "none")};
  background-color: transparent;
  border: none;
  outline: none;
  color: white;
`;

const SearchBar = () => {
    const { items, setItems } = useContext(GildedContext)

    const [input, setInput] = useState<string>("");
    const [barOpened, setBarOpened] = useState<boolean>(false);
    const formRef = useRef( ) as MutableRefObject<HTMLFormElement>
    const inputFocus = useRef()  as MutableRefObject<HTMLInputElement>;



    const onFormSubmit = (e: React.FormEvent) => {
        // When form submited, clear input, close the searchbar and do something with input
        e.preventDefault();
        setInput("");
        setBarOpened(false);

        //_____ implementing  leven sort
        var array : string[] = []
        var ItemsList : ItemModel[] = []

        for (const i of items){
           array.push(new ItemModel(i).toString())
        }

        let levSorted = levenSort(array, input.toLowerCase())


        for (var name  of levSorted) {
            var split = name.split(' _ ')

            ItemsList.push(new ItemModel({name : split[0] , quality : Number(split[1]) , sellIn:Number(split[2]) , id:Number(split[3])}))
        }

        setItems(ItemsList)

    };
    useEffect(() => {
        formRef.current.value = "";
    }, []);

    return (
        <div className="App">
            <Form
                onClick={() => {
                    // When form clicked, set state of baropened to true and focus the input
                    setBarOpened(true);
                    inputFocus.current.focus();
                }}
                // on focus open search bar
                onFocus={() => {
                    setBarOpened(true);
                    inputFocus.current.focus();
                }}
                // on blur close search bar
                onBlur={() => {
                    setBarOpened(false);
                }}
                // On submit, call the onFormSubmit function
                onSubmit={onFormSubmit}
                ref={formRef}
                barOpened={barOpened}

            >
                <Button type="submit" barOpened={barOpened}>
                    <Searchicon/>
                </Button>
                <Input
                    onChange={e => setInput(e.target.value)}
                    ref={inputFocus}
                    value={input}
                    barOpened={barOpened}
                    placeholder="Search for item and press (( Enter ))... "
                />
            </Form>
        </div>
    );
}

export default SearchBar
