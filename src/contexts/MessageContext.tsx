import { createContext, useState } from 'react';
import { MessageSnakBar } from '../components';
import {ItemModel} from "../model/ItemModel";

const MessageContext = createContext<{ AddMessage: (ItemModel: ItemModel) => void; DeletedMessage: (ItemModel: ItemModel) => void; toggle: () => void }>({ DeletedMessage: () => { }, AddMessage: () => { }, toggle: () => { } });
/***
 *
 * context for handling messages through app for snak bar
 * @param children
 * @constructor
 */
const MessageProvider = ({ children }: any) => {
    const [toast, setMessage] = useState<string>('');
    const [isActive, setIsActive] = useState<boolean>(false);


    const AddMessage = (item: ItemModel) => {
        setMessage(`${item.name} added`);
        setIsActive(true);

    }
    const DeletedMessage = (item: ItemModel) => {
        setMessage(`${item.name} deleted`);
        setIsActive(true);

    }

    const toggle = () => {
        setIsActive(false);
    }
    return (
        <MessageContext.Provider value={{
            AddMessage,
            DeletedMessage,
            toggle
        }}>
            <MessageSnakBar isActive={isActive} message={toast} />
            {children}
        </MessageContext.Provider>
    );
}



export { MessageProvider, MessageContext }