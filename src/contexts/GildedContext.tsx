import { createContext, useState } from "react";
import {ItemModel,initialState} from "../model/ItemModel";


const GildedContext = createContext<{ items: Array<ItemModel>, setItems: React.Dispatch<React.SetStateAction<ItemModel[]>> }>({ items: [], setItems: () => { } })
/***
 *
 * context for handling items
 * @param children
 * @constructor
 */
const GildedProvider = ({ children }: any) => {

    const [items, setItems] = useState<Array<ItemModel>>(initialState)

    const val = { items, setItems }
    return (
        <GildedContext.Provider value={val}>
            {children}
        </GildedContext.Provider >
    )

}
export { GildedProvider, GildedContext };